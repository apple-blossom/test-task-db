﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace DeutscheBahnTestTask.Base
{
    class BasePage
    {
        public IWebDriver WebDriver;

        public WebDriverWait Wait(int time = 10) => new WebDriverWait(WebDriver, TimeSpan.FromSeconds(time));

        public void WaitForElementToLoad(By waitFor, int time = 10)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(waitFor));
        }

        public void WaitForElementToBeClickable(By waitFor, int time = 10)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(waitFor));
        }

        public void WaitForElementToDisappear(By waitFor, int time = 10)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(waitFor));
        }

        public void WaitForAjax(int timeoutSecs = 10, bool throwException = false)
        {
            for (var i = 0; i < timeoutSecs; i++)
            {
                var ajaxIsComplete = (bool)(WebDriver as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0");
                if (ajaxIsComplete) return;
                Thread.Sleep(1000);
            }
            if (throwException)
            {
                throw new Exception("WebDriver timed out waiting for AJAX call to complete");
            }
        }


    }
}

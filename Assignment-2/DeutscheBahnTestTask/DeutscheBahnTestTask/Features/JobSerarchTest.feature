﻿Feature: JobSerarchTest
	As an applicant 
	I want to find all developer positions in Frankfurt am Main
	so that I can apply for a position.


Scenario Outline: Search for a job and check the result
	Given I open the main page
	And I click on job search page
	And I fill in a <jobTitle>, a <department>, a <city> in a search form
	When the result with <jobTitle> should be shown I choose a job opportunity from the list
	Then the correct job should be opened
	And I click apply button
	And the button shows correct information <email>

Examples: 
| jobTitle  | department | city				 | email                          |
| Developer | IT         | Frankfurt (Main)  | dbz-bewerbung@deutschebahn.com |

Scenario Outline: Search for a job negative test
	Given I open the main page
	And I click on job search page
	When I fill in a <jobTitle>, a <department>, a <city> in a search form
	Then the result should not contain data

Examples: 
| jobTitle     | department | city				 |
| negativeTest | IT         | Frankfurt (Main)  |
﻿using DeutscheBahnTestTask.Helpers;
using DeutscheBahnTestTask.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace DeutscheBahnTestTask.StepDefinitions
{
    [Binding]
    public class JobSerarchTestSteps
    {
        string JobTitle;
        MainPage mainPage;
        SearchPage searchPage;
        JobClaimPage jobClaimPage;
        IWebDriver driver;

        [BeforeScenario]
        public void BeforeScenario()
        {
            driver = new ChromeDriver();
            mainPage = new MainPage(driver);
            searchPage = new SearchPage(driver);
            jobClaimPage = new JobClaimPage(driver);
        }

        [AfterScenario]
        public void AfterScenario()
        {
            driver.Dispose();
        }

        [Given(@"I open the main page")]
        public void GivenIOpenTheMainPage()
        {
            mainPage.OpenMainPage(DataHelper.BaseURI);
        }
        
        [Given(@"I click on job search page")]
        public void GivenIClickOnJobSearchPage()
        {
            mainPage.HoverOverMenuItemAndOpenJobSearch();
        }

        [Given(@"I fill in a (.*), a (.*), a (.*) in a search form")]
        [When(@"I fill in a (.*), a (.*), a (.*) in a search form")]
        public void WhenIFillInASearchForm(string job, string department, string city)
        {
            searchPage.FillInSearchForm(job, department, city);
        }

        [When(@"the result with (.*) should be shown I choose a job opportunity from the list")]
        public void WhenTheResultWithJobShouldBeShownIChooseAJobOpportunityFromTheList(string job)
        {
            JobTitle = searchPage.CheckSearchResultsAndChooseFirstJob(job);
        }

        [Then(@"the correct job should be opened")]
        public void ThenTheCorrectJobShouldBeOpened()
        {
            jobClaimPage.CheckJobTitleIsCorrect(JobTitle);
        }

        [Then(@"I click apply button")]
        public void ThenIClickApplyButton()
        {
            jobClaimPage.ClickApplyButton();
        }
        
        [Then(@"the button shows correct information (.*)")]
        public void ThenTheButtonShowsCorrectInformation(string email)
        {
            jobClaimPage.VerifyApplyContent(email);
        }


        [Then(@"the result should not contain data")]
        public void ThenTheResultShouldNotContainData()
        {
            searchPage.CheckSearchResultsAndVerifyItIsEmpty();
        }


    }
}

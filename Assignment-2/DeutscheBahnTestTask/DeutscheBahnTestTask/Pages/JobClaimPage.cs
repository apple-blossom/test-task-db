﻿using DeutscheBahnTestTask.Base;
using NUnit.Framework;
using OpenQA.Selenium;

namespace DeutscheBahnTestTask.Pages
{
    class JobClaimPage : BasePage
    {
        public static By JobOpportunityTitle = By.TagName("h1");
        public static By ApplyButton = By.Id("applyNow");
        public static By ApplyContent = By.ClassName("apply-now-content");
        public static By ApplyContentEmail = By.XPath("//p[contains(@class,'apply-now-content')]//a[contains(@href,'mailto')]");

        public JobClaimPage(IWebDriver WebDriver)
        {
              this.WebDriver = WebDriver;
        }

        public void CheckJobTitleIsCorrect(string jobTitle)
        {
            StringAssert.Contains(jobTitle, WebDriver.FindElement(JobOpportunityTitle).Text);
        }

        public void ClickApplyButton()
        {
            WebDriver.FindElement(ApplyButton).Click();
            WaitForElementToLoad(ApplyContent);
        }

        public void VerifyApplyContent(string email)
        {
            Assert.AreEqual(email, WebDriver.FindElement(ApplyContentEmail).Text);
        }
    }
}

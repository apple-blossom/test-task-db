﻿using DeutscheBahnTestTask.Base;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace DeutscheBahnTestTask.Pages
{
    class SearchPage : BasePage
    {
        public static By JobSearchInput = By.Id("searchtext");
        public static By DepartmentChooseDropdown = By.CssSelector("#jobCategory+div button span");
        public static By DepartmentDropdownSelected = By.CssSelector("option[selected='selected']");
        public static By CityChooseDropdown = By.CssSelector("#cityName+div button span");
        public static By DropDownItemWithText(string text) => By.XPath($"//li/a/span[contains(text(),'{text}')]");
        public static By JobsResultList = By.CssSelector(".suche-kontext-filter-facet+.list-group");
        public static By JobsResultListItems = By.CssSelector(".suche-kontext-filter-facet+.list-group a li");

        public SearchPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
        }

        public void FillInSearchForm(string job, string department, string city)
        {
            var departmentElement = DropDownItemWithText(department);
            var cityElement = DropDownItemWithText(city);
            WebDriver.FindElement(DepartmentChooseDropdown).Click();
            WaitForElementToLoad(departmentElement);
            WebDriver.FindElement(departmentElement).Click();
            WaitForElementToBeClickable(DepartmentChooseDropdown);
            Assert.AreEqual(department, WebDriver.FindElement(DepartmentChooseDropdown).Text);
            WaitForElementToBeClickable(DepartmentChooseDropdown);
            WebDriver.FindElement(CityChooseDropdown).Click();
            try
            {
                WaitForElementToBeClickable(cityElement, 3);
            }
            catch (WebDriverTimeoutException)
            {
                WebDriver.FindElement(CityChooseDropdown).Click();
            }
            WebDriver.FindElement(cityElement).Click();
            Assert.AreEqual(city, WebDriver.FindElement(CityChooseDropdown).Text);
            WebDriver.FindElement(JobSearchInput).SendKeys(job);
            WebDriver.FindElement(JobSearchInput).SendKeys(Keys.Enter);
            WaitForAjax();
        }

        public string CheckSearchResultsAndChooseFirstJob(string job)
        {
            var results = WebDriver.FindElements(JobsResultListItems);
            foreach (var item in results)
            {
                StringAssert.Contains(job.ToLower(), item.Text.ToLower());
            }
            WaitForAjax();
            var text = results[0].Text;
            results[0].Click();
            WaitForElementToLoad(JobClaimPage.ApplyButton);
            return text;
        }

        public void CheckSearchResultsAndVerifyItIsEmpty()
        {
            var results = WebDriver.FindElements(JobsResultListItems);
            Assert.AreEqual(0, results.Count);
        }

    }
}

﻿using DeutscheBahnTestTask.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;

namespace DeutscheBahnTestTask.Pages
{
    class MainPage : BasePage
    {
        public static By ClaimsMenuItem =  By.CssSelector("a[title='Für Bewerber']");
        public static By JobSearchMenuItem =  By.XPath("//a[contains(text(),'Jobsuche') and contains(@class,'backtag')]");

        public MainPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
        }

        public void OpenMainPage(string URL)
        {
            WebDriver.Navigate().GoToUrl(URL);
            WaitForElementToLoad(ClaimsMenuItem);
        }

        public void HoverOverMenuItemAndOpenJobSearch()
        {
            var element = Wait(5).Until(ExpectedConditions.ElementIsVisible(ClaimsMenuItem));
            Actions action = new Actions(WebDriver);
            action.MoveToElement(element).Perform();
            WebDriver.FindElement(JobSearchMenuItem).Click();
            WaitForElementToLoad(SearchPage.DepartmentChooseDropdown);
        }

    }
}

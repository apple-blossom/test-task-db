# Test Automation Assignment DB Zeitarbeit

## Instructions:
The first task is in Assignment-1.txt file
The second is in Assignment-2 folder

To run the tests, go to ./Assignment-2/DeutscheBahnTestTask and open the solution via Visual Studio, restore NuGet packages and Build the solution. Then you can run the tests

### Requirements:
- Visual Studio 2019
- Windows 10

==========================

## Introduction
Thank you for working on our test automation assignment.
It consists of two assignments, which give you a better possibility to demonstrate your testing and automation skills and us a better insight in the style of your work.
We hope you will like it and have some fun with the tasks.
Please let us know, if you have questions or need anything else from us and we will be happy to answer.

## Assignment 1
What type of project are you currently working on, how do you decide what to test in this project, which test methods are you using for which pieces of the project? Please briefly describe how your test strategy looks like. 

*Note: you should not provide any details about the content of your project, but only about the testing aspects.*

## Assignment 2
DB Zeitarbeit GmbH is an expert in individual staffing solutions with the aim to provide the best personnel at the time and place they are needed. 

Through our [website](https://www.dbzeitarbeit.de/) the potential applicants can inform themselves about our company and services. Also, a search site is existing where the candidate can search for open positions and apply for it.
The search Site is reachable from the home page via the path 
> "Für Bewerber->Jobsuche" 

(uri path: `/zeitarbeit-de/bewerber/jobs-deutsche-bahn`)

Now, imagine you are part of an agile team and working in the sprint on the following **user story**:

---
As an applicant I want to find all developer positions in Frankfurt am Main, so that I can apply for a position.

* search page is used for the search
* correct amounts of open positions are found
* a click on a position result leads to a subpage, which displays the correct job title
* on the subpage the button `"Jetzt Bewerben"` can be clicked 
    and reveals the application e-mail `"dbz-bewerbung@deutschebahn.com"`
---

### Assignment Task:
Conceive a couple of useful test cases and automate them as keyword, data or behavioral driven test cases.
The test cases should be executable.
The test cases should be stored in the provided git repository.

### Remarks:
* As you cannot influence or create a specific precondition of the  SUT you can adapt your test case according to the actual state of amount of search results, the layout of the website, changing texts of title, buttons etc. and more
* You can choose a test framework of your choice
The example makes use of [Robot Framework](www.robotframework.org) with [SeleniumLibrary](https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html) as a low level keyword provider.
* The Robot Test file `Test_searching_job_positions.robot` gives an executable skeleton of a robot framework test case
* The files in the folder `results` where generated by running the test case script in the following way: 
        
        robot --outputdir .\results .\Test_searching_job_positions.robot
* Precondition for running Selenium Tests is a installed Webdriver like [ChromeDriver](https://chromedriver.chromium.org/)
* The provided Git repository bundle can be cloned like a normal repository from a network location:
        
        git clone .\<name of the bundle> .\<destination folder>

